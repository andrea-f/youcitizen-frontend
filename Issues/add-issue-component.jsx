import React from 'react'
import Utils from '../utils-component'
import IssueDetail from './issue-detail-component'
import {
  Link
} from 'react-router-dom'

class AddIssue extends React.Component {
    constructor() {
  		super();
      this.state = {
        image64    : null,
        created    : false,
        error      : null,
        updated    : false
      }
  		this.handleSubmit = this.handleSubmit.bind(this)
      this.whatToShow = this.whatToShow.bind(this)
      this.utils = new Utils
	  }

    componentDidMount() {
      if (typeof this.props.issue !== "undefined") {
        this.refs.title.value               = this.props.issue.title
        this.refs.description.value         = this.props.issue.description || ''
        this.refs.location.value            = this.props.issue.location
        this.refs.submit.value              = "Update"
        if (typeof this.props.issue.tags !== 'undefined') {
            this.refs.tags.value            = this.props.issue.tags
        }
      } 
    }

    /*
     * Handles form submit button
     */
    handleSubmit(e) {
        e.preventDefault()
        let issue = {
          "title"      : this.refs.title.value,
          "description": this.refs.description.value,
          "location"   : this.refs.location.value,
          "tags"       : this.refs.tags.value
        }
        
        // Make sure we have an image
        if ((e.nativeEvent.target.children[4]) && 
          (typeof e.nativeEvent.target.children[4].files !== 'undefined') &&
         (e.nativeEvent.target.children[4].files.length > 0)) {
            this.utils.getBase64(e.nativeEvent.target.children[4].files[0]).then((data) => {
              issue['image'] = data
              // What to do with the data
              this.updateOrCreate(issue)
            })
        } else {
            this.updateOrCreate(issue)
        }
    }

    /*
     * Either create or update an issue
     */
    updateOrCreate(issue) {
        let url
        const want_to_update = this.props.want_to_update || false
        if (!want_to_update) {
            url = this.utils.urls.issue.add
            this.createIssue(url, issue)
        } else {
            url = this.utils.urls.issue.update + this.props.issue.id + "/"
            this.updateIssue(url, issue)
        }
    }

    /* 
     * Updates remote resource
     */
    updateIssue(url, issue) {
      this.utils.updateResource(url, issue).then((data) => {
          if ((typeof data !== "undefined") && (typeof data.title !== "undefined")) {
              // Successful issue creation
              this.setState({
                updated: true,
                issue_id: data.id
              })
              //console.log("Issue updated")

            } else {
              const error_str = this.utils.formatError(data.response.data)
              this.setState({error: error_str})
            }
      });
    }

    /*
     * Handles fetching data via utils
     */
    createIssue(url, issue) {
      
      this.utils.postResource(url, issue).then((data) => {
          if ((typeof data !== "undefined") && (typeof data.title !== "undefined")) {
            // Successful issue creation
            this.setState({created: true})
            this.props.updateCallback(data)
            

            } else {
              console.log("Didnt create issue")
              console.log(data.response)
              const error_str = this.utils.formatError(data.response.data)
              this.setState({error: error_str})
            }
      })      
    }

    whatToShow() {
      let disp
      

      const add_issue_form = (
            <form onSubmit={this.handleSubmit} ref="add_issue_form">
              <div className="form-group">
                <label htmlFor="title">Issue title:</label>
                <input type="text" placeholder="Give a title to your issue" ref="title" name="title" id='title' className="form-control" />
              </div>
              <div className="form-group">
                <label htmlFor="description">Issue description:</label>
                <input type="text" placeholder="Give a description to your issue" ref="description" name="description" id='description' className="form-control" />
              </div>
              <div className="form-group">
                <label htmlFor="location">Issue location:</label>
                <input type="text" placeholder="Where did this issue materialize" ref="location" name="location" id='location' className="form-control" />
              </div>
              <label>Issue image:</label>
                <input type="file" ref="image" id="image" name="image" className="form-group" />
              <div className="form-group">
                <label htmlFor="tags">Issue tags:</label>
                <input type="text" placeholder="Holes in the road, trash, abandoned building" ref="tags" name="tags" id='tags' className="form-control" />
              </div>
              <input type="submit" id="submit" ref="submit" className="btn btn-success" /> 
            </form>          
        ) 
      
      // Inform the user that the issue was created
      if (this.state.created) {
          disp = (<pre>Issue created.</pre>)
      } else if (this.state.error) {
          // Or inform them there was an error
          disp = (<div><pre>{this.state.error}</pre>{add_issue_form}</div>)
      } else {
          // Finally just show the form
          disp = add_issue_form
      }

      // Tell the user the issue was correctly updated
      // And override previous
      if (this.state.updated) {
          disp = (
            <div>
              <pre>Issue updated.</pre>
              <pre>
              <Link to={{
                  pathname: `/profile/`
              }}>
                  Back to profile.</Link>
              </pre>
            </div>
          )
      }
      return disp
    }

    /*
     * Renders the HTML for the form
     */
    render() {

      const disp = this.whatToShow()
      
      const disp_bordered = (
            <div 
            className="col-xs-12 col-lg-12 wrapper spacing-button">
            {disp}
            </div>
        )
    	return disp_bordered
    }
}

export default AddIssue