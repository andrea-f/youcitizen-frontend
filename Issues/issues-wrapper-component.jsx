import React from 'react'
import ReactDOM from 'react-dom'
import Issues from './issue-list-component'
import Utils from '../utils-component'
import AddIssue from './add-issue-component'
import NavButton from '../Nav/navbutton-component'

class IssuesWrapper extends React.Component {

	constructor(props) {
      super(props);
      this.state = { 
      	data          : [],
      	show_add_issue: false,
      	request_returned: false
      }
      this.updateCallback = this.updateCallback.bind(this)
      this.openAddIssue   = this.openAddIssue.bind(this)
      this.whatToShow = this.whatToShow.bind(this)
      this.utils = new Utils
    }

    // Gets called when an issue has been added
	updateCallback(resp) {
		const new_data = this.state.data
		this.setState({
			data:new_data,
			request_returned: true
		})
	}

	// Toggle add issue form, if user is logged in.
	openAddIssue() {
		const toggle = !this.state.show_add_issue
		this.setState({show_add_issue: toggle})
	}

	componentDidMount() {
        // Fetch issues
        let url = this.utils.urls.issue.every
        // Add filter if present with request
        if (typeof this.props.filter !== "undefined") {
        	url = url + (this.props.filter) + "/"
    	}
    	
	   	this.utils.getResource(url).then((data) => {
	   		this.setState({
	   			data: data.results,
	   			request_returned: true
	   		}) 
        }).catch((err)=>{
        	//this.utils.logout()
			this.setState({
	   			request_returned: true
	   		})

        })
    }

    whatToShow() {
		const add_issue = (
			<div id="container">
	    		<div id="row">
		    		<NavButton buttonAction={this.openAddIssue} class_name="btn btn-success spacing-button btn-lg" button_text={"Add Issue"} />
		    	</div>
		    	{
		    		(this.state.show_add_issue)?
		    		<div id="row">
		    			<AddIssue 
		    				updateCallback={this.updateCallback} />
		    		</div>:
		    		null
		    	}
	    	</div>
	    )

		

		return (
			<div className="container">
				{
					(this.utils.isLoggedIn() && this.utils.getUsername())?
        			add_issue:
        			null
        		}
				<div className="row">
	    			<Issues data={this.state.data} />
	    		</div>
	    		
    		</div>
    	)
    }

	render() {
		return (!this.state.request_returned?
			<div><i className="fa fa-spinner fa-spin" style={{fontSize:24+"px"}}></i></div>:
			<div>{this.whatToShow()}</div>
		)
	}
}

export default IssuesWrapper


