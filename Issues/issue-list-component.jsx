import React from 'react'
import Utils from '../utils-component'
import {
  Link
} from 'react-router-dom'

class Issues extends React.Component {

    constructor() {
      super();
      this.utils = new Utils
      this.whatToShow = this.whatToShow.bind(this)
    }

    whatToShow() {

        let issues
            if (this.props.data && this.props.data.length > 0) {
                issues = (
                    <ul className="list-group list-group-horizontal " style={{width:100+"%",float:"left"}}>
                    { 
                        this.props.data.map(issue => (
                            
                            <li className="issue list-group-item" key={issue.id} style={{width:30+"%",padding:1+"%", margin:1+"%", textAlign:"center",display:"inline-flex"}}>
                                
                                <div className="card">
                                    <div className="card-header">
                                    <Link to={{
                                        pathname: `/issue/${issue.id}`,
                                        state: {issue: issue}
                                    }}>
                                        <img className="card-img-top img-responsive" 
                                            src={issue.image} />                        
                                        <h3 className="card-title">{issue.title}</h3>
                                    </Link>            
                                    </div>
                                    <div className="card-block">
                                        <h4 className="card-text">{issue.description}</h4>
                                        <h4 className="card-text">{issue.location}</h4>
                                        <h4 className="card-text">{issue.creator}</h4>
                                        <h4 className="card-text">Comments: {issue.comments.length}</h4>
                                    </div>
                                </div>
                                
                            </li>
                            
                        ))
                    }
                    </ul>
                )
            } else {
                issues = (<h4>No issues in database</h4>)
            }  
        return issues      
    }

    render() {
            
        const issues = this.whatToShow()

            // Add a pretty printed div
            const issue_bordered = (
                <div 
                    className="col-xs-12 col-lg-12 black_box" 
                    style={{
                        padding: "auto",
                        margin: "auto"
                    }}>
                    {issues}
                </div>
            )
        return (
            <div>
            {issue_bordered}
            </div>
        )
    }
}

Issues.propTypes = {
  data: React.PropTypes.array
}


export default Issues


