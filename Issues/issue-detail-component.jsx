import React from 'react'
import Utils from '../utils-component'
import Comments from './comments-component'
import AddIssue from './add-issue-component'
import {
  Link
} from 'react-router-dom'
import { Redirect } from 'react-router'

class IssueDetail extends React.Component {
    constructor(props, context) {
      super(props);
      this.state = { 
        issue         : {},
        username      : null,
        deleted       : false,
        want_to_update: false
      }
      this.utils = new Utils
      this.deleteIssue = this.deleteIssue.bind(this)
      this.updateIssue = this.updateIssue.bind(this)
      this.splitTime = this.splitTime.bind(this)
      this.whatToShow = this.whatToShow.bind(this)
    }



    componentDidMount() {
        // Disambiguate how the data was passed in
        // if via props or url parameter
        try { 
            let issue = this.props.issue //|| this.props.location.state.issue 
            this.context.issue_id = this.props.issue.id
            this.setState({issue: issue})
        } catch(err) {
            let id
            try{
                id = this.props.match.params.id
            } catch(err) {
                // If id  is not defined
                // default to 1
                id = 1
            }
            this.context.issue_id = id 
            // Fetch to remotely
            this.fetchData(this.context.issue_id)
        }
    }

    fetchData(iss) {
        const url  = this.utils.urls.issue.detail + iss + "/"
        this.utils.getResource(url).then((data) => {
            if ((typeof data !== "undefined")) {
                this.setState({
                    issue: data
                })
                
            } else {
                this.utils.showError(data)
            }

        })
    }

    updateIssue() {
        this.setState({want_to_update: true})
    }

    deleteIssue() {
        const url = this.utils.urls.issue.detail+this.state.issue.id
        this.utils.deleteResource(url).then((data) => {
            if ((typeof data !== "undefined")) {
                this.setState({deleted: true})
            } else {
                this.utils.showError(data)
            }
        });
    }

    splitTime(created) {
        if (typeof created !== 'undefined') {
            return created.split("T")[0]
        }
    }

    whatToShow() {
        
        // Redirect to home on delete successful
        if (this.state.deleted) {
            return (<Redirect to={{
                      pathname: '/profile/',
                      state: { username: this.state.issue.creator }
            }}  />)
        }
        // Pass issue to update form
        if (this.state.want_to_update) {
            return (
                <AddIssue 
                    issue={this.state.issue} want_to_update={true} />
            )
        }

        const issue_actions = (
            <div className="row">
                <button onClick={this.deleteIssue} className="btn btn-warning user-actions" ref="delete" id="delete">Delete issue</button>
                <button onClick={this.updateIssue} className="btn btn-warning user-actions" ref="update" id="update">Update issue</button>
            </div>
        )
        const maps_url = this.utils.urls.maps_api_url+this.utils.urls.maps_api_key+"&q="+this.state.issue.location
        
        const issue = (

            <div className="card">
                <div className="card-header">
                    <h3 className="card-title issue-detail-header">{this.state.issue.title}</h3>
                    <div className="row">
                    { 
                        (this.state.issue.creator === this.utils.getUsername()) ?
                        issue_actions:
                        null
                    }
                    </div>
                </div>
                <div className="card-block">
                    <div className="row">
                        <table style={{width:100+"%"}}>
                            <tbody>
                            <tr>
                              <td style={{width:50+"%"}}>
                                <h4 className="card-text issue-detail">Location: {this.state.issue.location}</h4>
                                <h4 className="card-text issue-detail">Creator: <Link to={`/profile/${this.state.issue.creator}`}>{this.state.issue.creator}</Link></h4>
                                <h4 className="card-text issue-detail">Created on: {this.splitTime(this.state.issue.created)}</h4>
                                <h4 className="card-text issue-detail">Description: {this.state.issue.description}</h4>
                                <h4 className="card-text issue-detail">Tags: {this.state.issue.tags}</h4>
                              </td>
                              <td>
                                <img className="card-img-right issue-detail-image" 
                                    src={this.state.issue.image} />
                              </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="row">
                        <h4 className="card-text detail-heading">Map:</h4>
                        <iframe
                          style={{border:1, width:100+"%",height:450+"px"}}
                          src={maps_url} />
                    </div>
                </div>
                <h4 className="card-text detail-heading">Comments:</h4>
                <Comments issue_id={this.state.issue.id} comments={this.state.issue.comments} />
            </div>
            
        )
        
        const issue_bordered = (
            <div 
            className="col-xs-12 col-lg-12" 
            style={{
                display:"block",
                borderStyle: "solid", 
                borderWidth: 5+"px", 
                padding: 2+"%", 
                margin: 2+"%"
            }}>
            {issue}
            </div>
        )
        // If the issue has been updated,
        // Don't show any border
        return (
            this.props.updated ?
            issue: issue_bordered
        )
    }

    render() {
        const disp = this.whatToShow()
        return disp       

            
    }
}

IssueDetail.contextTypes = {
  router  : React.PropTypes.object.isRequired,
  username: React.PropTypes.string,
  issue_id: React.PropTypes.number
}

export default IssueDetail;

