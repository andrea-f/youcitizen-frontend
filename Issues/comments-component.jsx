import React from 'react';
import ReactDOM from 'react-dom';
import Issues from './issue-list-component';
import Utils from '../utils-component'
import AddIssue from './add-issue-component'
import NavButton from '../Nav/navbutton-component'
import { Redirect } from 'react-router'

class Comments extends React.Component {

	constructor(props) {
      super(props);
      this.state = { 
      	comments_data   : [],
      	show_add_comment: false,
      	comment_added   : false
      }
      this.openAddComment = this.openAddComment.bind(this)
      this.handleSubmit = this.handleSubmit.bind(this)
      this.whatToShow = this.whatToShow.bind(this)
      this.utils = new Utils

    }

	openAddComment() {
		const toggle = !this.state.show_add_comment
		this.setState({show_add_comment: toggle})
	}

    handleSubmit(e) {
    	e.preventDefault()

    	const comment = {
          "text" : this.refs.comment.value,
          "issue": this.props.issue_id
        }
        const url = this.utils.urls.issue.comment + this.props.issue_id + "/"
        this.utils.postResource(url, comment).then((data) => {
        	if ((typeof data !== "undefined") && 
        		(typeof data.creator !== 'undefined')) {
        		
        		// Add newly created comment to comments list
                let comments = this.state.comments_data
                comments.push(data)
        		this.setState({
                    comment_added: true,
                    username     : data.creator,
                    comments_data: comments
                })

            // Something went wrong
            } else {
                const error_str = this.utils.formatError(data.response.data)
                //console.log(error_str)
                this.setState({error: error_str})
            }
        })

    }


    componentWillReceiveProps(props) {
    	// Update comments list if new comment has been added
    	if (typeof props.comments !== 'undefined') {
    		this.setState({
    			comments_data: props.comments
    		})
    	}
    }

    componentDidMount() {
    	// Make sure we have some comments to display
		(typeof this.props.comments !== 'undefined' && this.props.comments.length> 0)?
		this.setState({
			comments_data: this.props.comments
		}): null
			
    }

  /*
   * Toggles add comment form
   */
	openAddComment() {
        this.setState({show_add_comment: !this.state.show_add_comment})
    }

   
    whatToShow() {
        const add_comment_form = (
            <form onSubmit={this.handleSubmit} ref="add_comment_form">
              <div className="form-group">
                <label htmlFor="comment">Your comment on this issue:</label>
                <textarea placeholder="Enter a comment" ref="comment" name="comment" id='comment' className="form-control" />
              </div>
              <input type="submit" id="submit" ref="submit" className="btn btn-success" /> 
            </form>          
        )

        const comments = (
            <ul className="list-group comments" style={{width:100+"%"}}>
                { 
                    this.state.comments_data.map(comment => (
                        <li className="comment list-group-item" key={comment.id}>
                            <h5>{comment.text}</h5>
                            <h5>User: {comment.creator}</h5>
                        </li>
                    ))
                }
            </ul>
        )


        return (
            <div className="card-block">
                {
                    (this.utils.isLoggedIn()) ? 
                    <button onClick={this.openAddComment} className="btn btn-warning user-actions" id="add_comment">Add Comment</button>:
                    null
                }

                {
                    (this.state.show_add_comment) ?
                    add_comment_form:
                    null
                }

                {
                    (this.state.comment_added) ?
                    <pre>Comment Added {this.state.username}!</pre>:
                    null
                }

                {
                    (this.state.error) ?
                    <pre>{this.state.error}</pre>:
                    null
                }

                {
                    (this.state.comments_data.length>0) ?
                    comments:
                    null
                }       
            </div>
        )

    }

	render() {
        const ui = this.whatToShow()
        return ui
	}
}

export default Comments;