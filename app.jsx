import React from 'react';
import ReactDOM from 'react-dom';
import IssuesWrapper from './Issues/issues-wrapper-component';
import IssueDetail from './Issues/issue-detail-component';
import AddIssue from './Issues/add-issue-component'
import Utils from './utils-component'
import ActivateUser from './Auth/activate-user-component'
import UserProfile from './Auth/user-profile-component'
import NavBar from './Nav/navbar-component'

import {
  BrowserRouter,
  browserHistory,
  Route,
  Link
} from 'react-router-dom';
import { Redirect } from 'react-router'

const header = (
	<header>
    <Link to={`/`}>
		<h1>YouCitizen</h1>
    </Link>
		<h2>Share what could be improved in the place you live.</h2>
    <h3>Create an account to create issues and comment on others.</h3>
    
	</header>
)


ReactDOM.render(

    (	
    	<div>
		    	<BrowserRouter history={browserHistory}>
		    		<div className="container"> 
            <NavBar />
            {header}
            
		    		<Route path="/issue/:id" component={IssueDetail} />
            <Route path="/profile/:username" component={UserProfile} />
            <Route exact path="/profile/" component={UserProfile} />
            <Route path="/activate/:code" component={ActivateUser} />
		    		<Route exact path="/" component={IssuesWrapper} />
			     </div>
		    	</BrowserRouter >

		    </div>
    ),
    document.querySelector('.container')

)