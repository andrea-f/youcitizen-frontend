import React from 'react';
import Utils from '../utils-component'
import Login from './login-component'
import IssuesWrapper from '../Issues/issues-wrapper-component'


class UserProfile extends React.Component {
    constructor(props) {
      super(props);
      this.state = { 
        activated: false,
        error    : null,
        username : "",
        user_id  : null,
        request_returned: false
      }
      this.utils = new Utils
      this.whatToShow = this.whatToShow.bind(this)
    }

    componentWillMount() {
        let username 
        try {
            // Get username from url
            username = this.props.match.params.username
        } catch (error) {
            // Get username from Router Link component
            username = this.props.location.state.username
        }
        // Fetch the user profile
        if (typeof username === 'undefined') {
            username = "me"
        } 
        this.getProfile(username)
    }

    getProfile(username) {
        // Get the user information
        const url  = this.utils.urls.user_profile + username
        this.utils.getResource(url).then((data) => {
            if (typeof data.id !== 'undefined')
            {
                this.setState({
                    // Set the state for some useful vars
                    username: data.username,
                    user_id : data.id
                })        
            } else {
                let e
                try {
                    e = data.response.data.error
                } catch (err) {
                    // In case there is a positive response
                    // but no id
                    e = data
                }

                this.setState({error: e})   
            }
        }).catch(error => {
            this.setState({error: "Error in fetching data"})
        })
        // Re render with response
        this.setState({request_returned: true})
    }

    whatToShow() {
        return (
            <div>
                {
                    this.state.username ?
                    <h1><pre>Issues created by: {this.state.username}</pre></h1>:
                    null

                }
                {
                    (this.state.user_id) ?
                    <IssuesWrapper filter={this.state.user_id} />:
                    <div>User created no issues.</div> 
                    }
            </div>
        )
    }
    
    render() {
        return (!this.state.request_returned?
            <i className="fa fa-spinner fa-spin" style={{fontSize:24+"px"}}></i>:
            <div>{this.whatToShow()}</div>
        )
                
    }
}

export default UserProfile;

