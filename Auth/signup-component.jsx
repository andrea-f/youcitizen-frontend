import React from 'react'
import Utils from '../utils-component'

class Signup extends React.Component {

	constructor() {
  		super();
  		this.state = {
  			username : null,
            error    : null,
            signed_up: false
  		}
        this.utils = new Utils
        this.whatToShow = this.whatToShow.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
	}


    // Handle click event on submit
    handleSubmit(e) {
        e.preventDefault()

        const url  = this.utils.urls.signup
        // Get submitted form data
        const signup_data = {
        	"username": this.refs.username.value,
        	"password": this.refs.password.value,
        	"email"   : this.refs.email.value
        }
        
        

        this.utils.postResource(url, signup_data).then((data) => {
            if ((typeof data !== "undefined") && 
        		(typeof data.username !== 'undefined')) {
        		// Activated key exists but its not active
                // Set just the username so it can be used in the rendered response
        		this.setState({
                    username: data.username,
                    signed_up: true
                })
            // Something went wrong
            } else {
                const error_str = this.utils.formatError(data.response.data)
                console.log("here2")
                this.setState({error: error_str})

            }
        }).catch(error => {
            console.log("here")
            const error_str = this.utils.formatError(data.response.data)
            this.setState({error: error_str})
        })
        
    }



    whatToShow() {
        let disp = null
        const signup_form = (
            <form onSubmit={this.handleSubmit} ref="signup_form" id="signup_form" >
            <div className="form-group">
                <input type="text" placeholder="username" ref="username" name="username" id='username' className="form-control" />
            </div>
            <div className="form-group">
                <input type="password" placeholder="password" ref="password" name="password" id='password' className="form-control" />
            </div>
            <div className="form-group">
                <input type="email" placeholder="email" ref="email" name="email" id='email' className="form-control" />
            </div>
                <input type="submit" id="submit" ref="submit" className="btn btn-success" />
            </form>
        ) 
        if (this.state.signed_up === true && typeof this.state.username != 'undefined') {
                disp = <pre>Hi {this.state.username} please confirm your email address to activate your account.</pre>
        } else {
            // If there was an error
            // during signup display the error

            //disp = signup_form
            let e
            this.state.error    ?
            e = <pre>{this.state.error}</pre>:
            e = null
            disp = <div>{e} {signup_form}</div>

        }
        return disp
    }

    render() {

        const disp = this.whatToShow()

        const disp_bordered = (
                <div 
                    className="col-xs-12 col-lg-12" 
                    style={{
                        display:"block",
                        borderStyle: "solid", 
                        borderWidth: 5+"px",
                        padding: 2+"%", 
                        margin: 2+"%"
                    }}>
                    {disp}
                </div>
        )
        return disp_bordered
    }
}

export default Signup