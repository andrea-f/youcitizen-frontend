import React from 'react'
import Utils from '../utils-component'
import Login from './login-component'

class ActivateUser extends React.Component {
    constructor(props) {
      super(props);
      this.state = { 
        email    : null,
        activated: false,
        error    : null
      }
      this.utils = new Utils
      this.whatToShow = this.whatToShow.bind(this)
    }

    componentWillReceiveProps(props){
        const code = props.match.params.code
        this.verifyAccount(code)
        
    }

    componentWillMount() {
        const code = this.props.match.params.code
        this.verifyAccount(code)
        // Call what to show
        
    }

    // Avoid putting everything in render() method
    // create ui here and then lazy load in to render
    whatToShow() {
        const active_account = (
            <div id="row">
                <pre>Thank you! Your account has been activated {this.state.email}, you can now log in:</pre>
                <Login />
            </div>
            
        )     
        const activation_error = (
            <div id="row">
                <pre>Your account was not activated because: {this.state.error}</pre>
            </div>
        )

        return (
            
                <div>
                {
                    this.state.activated ?
                    active_account       :
                    activation_error
                }
                </div>
            
            
        )   
    }

    // Checks with back end that activation key is valid
    // If it is account is activated
    verifyAccount(code) {
        const url  = this.utils.urls.activate_user
        const data = {
            "activation_key": code
        }
        this.utils.postResource(url, data).then((data) => {
            if ((typeof data !== "undefined") && (typeof data.email !== 'undefined')) {
                this.setState({email:data.email, activated: true})
            } else {
                this.setState({error: data.response.data.error})
                console.log("Activation failed")
            }
        });
    }

    render() {
        const ui = this.whatToShow()
        return ui
    }
}

export default ActivateUser

