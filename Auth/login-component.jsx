import React from 'react'
import Utils from '../utils-component'
import { Redirect } from 'react-router'

class Login extends React.Component {
    constructor() {
  		super()
  		this.handleSubmit = this.handleSubmit.bind(this)
        this.state = {
            error: null,
            logged_in: false
        }
        this.utils = new Utils
        this.whatToShow = this.whatToShow.bind(this)
	}

    handleSubmit(e) {
        e.preventDefault()
        const url = this.utils.urls.login 
        const login_data = {
        	"username": this.refs.username.value,
        	"password": this.refs.password.value
        }
        if (login_data.username.length === 0 || login_data.password.length === 0){
            this.setState({error: "Both password and username must not be blank."})
        } else {
            this.utils.postResource(url, login_data).then((data) => {
                if ((typeof data !== "undefined") && (typeof data.token !== "undefined")) {
            		// Successful login, store in localStorage
                	this.utils.setLoginToken(data.token)
                    // And username in session storage
                    this.utils.setUsername(data.username)
                    this.setState({logged_in: true})
                } else {
                    try {
                        this.setState({error: data.response.data.non_field_errors[0]})
                    } catch(err) {
                        const e = "("+data.response.status+") Error in login! "
                        this.setState({error: e})
                    }
                    
                }
                
            }).catch(error => {
                this.setState({error: error})
            })
        }
        
    }

    whatToShow() {
        const login_form = (
            <form onSubmit={this.handleSubmit} ref="login_form">
                <div className="form-group">
                    <label htmlFor="username">Username:</label>
                    <input type="text" placeholder="username" ref="username" name="username" id='username' className="form-control" />
                </div>
                <div className="form-group">
                    <label htmlFor="password">Password:</label>
                    <input type="password" placeholder="password" ref="password" name="password" id='password' className="form-control" />
                </div>
                    <input type="submit" id="submit" ref="submit" className="btn btn-success" />
            </form>
        ) 

        const not_logged_in = this.utils.showError(this.state.error)
        
        // Dynamic templating var
        let disp = null
        {
            if (this.state.logged_in) {
                // Refresh the page if login is successful.
                return (<Redirect to={{
                    pathname: '/'
                }}  />)
            } else if (this.state.error) {
                // Otherwise show error in login and login form
                disp = (<div className="col-xs-12 col-lg-12 black_box" >{not_logged_in} {login_form} </div>)
            } else {
                // If the user has not tried to login yet 
                // show login form
                disp = (<div className="col-xs-12 col-lg-12 black_box" >{login_form}</div>)
            }   
        }
        return disp
    }

    render() {

        const disp = this.whatToShow()
        return disp
    }
}


export default Login




