import React from 'react'
import Utils from '../utils-component'
import {
  Link
} from 'react-router-dom'

class NavButton extends React.Component {
	constructor() {
		super()
		this.utils = new Utils
	}
	render() {
		const link_to = this.props.link_to || false
		const class_name = this.props.class_name || this.utils.css.button_class
		let disp = null
		const button = (
			<button className={class_name} onClick={this.props.buttonAction}>
				{this.props.button_text}
			</button>
		)
		this.props.link_to ?
		disp = (
			<Link to={this.props.link_to}>
				{button}
			</Link>
		):  disp = button

		return (disp)     	
	}
}

NavButton.propTypes = {
  buttonAction: React.PropTypes.func,
  button_text : React.PropTypes.string.isRequired,
  link_to     : React.PropTypes.string,
  class_name  : React.PropTypes.string
}

export default NavButton