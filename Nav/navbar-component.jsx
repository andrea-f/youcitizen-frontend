import React from 'react';
import Utils from '../utils-component'
import NavButton from './navbutton-component'
import Login from '../Auth/login-component'
import Signup from '../Auth/signup-component'
import { Redirect } from 'react-router'
console.error = () => {}

class NavBar extends React.Component {

	constructor() {
      super();
      this.utils = new Utils
      this.state = { 
      	show_login : false,
        show_signup: false,
        logged_in  : this.utils.isLoggedIn(),
        did_logout : false
      }
      this.openLogin    = this.openLogin.bind(this)
      this.openSignup   = this.openSignup.bind(this)
      this.logout       = this.logout.bind(this)
      this.loggedOutNav = this.loggedOutNav.bind(this)
      this.loggedInNav  = this.loggedInNav.bind(this)
      this.whatToShow = this.whatToShow.bind(this)
    }


  /*
   * Toggles login form
   */
	openLogin() {
        this.setState({show_login: !this.state.show_login})
    }

  /*
   * Toggles signup form
   */    
  openSignup() {
      this.setState({show_signup: !this.state.show_signup})
  }


  logout(e) {
    e.preventDefault()
    /*
     * Clear localStorage as a way to log out and session storage too.
     */
     console.log("LOGOUT")
    this.utils.logout()
    this.setState({
      did_logout: true,
      show_signup: false,
      show_login : false
    })
  }

 /*
  * Navigation bars
  */


  loggedOutNav() {

    return (
        <div>
          <NavButton 
                    class_name="btn btn-success btn-lg spacing logged_out" 
                    buttonAction={this.openLogin} 
                    button_text={"Login"} />  
                  <NavButton 
                    class_name="btn btn-success btn-lg spacing logged_out" 
                    buttonAction={this.openSignup} 
                    button_text={"Signup"} />

      </div>     
      
    )
  }

  loggedInNav() {

    return (
      <div className="navbar-custom">
          <NavButton link_to={`/profile/`} 
            class_name="btn btn-success btn-lg spacing" 
            button_text={"Profile"} />
          <NavButton 
            class_name="btn btn-success btn-lg spacing" 
            buttonAction={this.logout} 
            button_text={"Logout"} />
      </div>
    )
  }

  whatToShow() {
    return (
     
      <div className="row align-items-start">
      {
        /* Refresh the page if the user has just logged out */
        this.state.did_logout ?
        (<Redirect to='/' />) :
        null
      }

      {
        /* Which navbar to show */
        this.utils.isLoggedIn() ?
        this.loggedInNav()      :
        this.loggedOutNav()
      }
      {
          /* Show signup form if user is not logged in
           * and has clicked on signup button
           */
          this.state.show_signup ?
          <div className="row"><Signup /> </div>            :
          null
      }
      {
          /* Show login form if user is not logged in
           * and has clicked on login button
           */
          this.state.show_login ?
          <div className="row">
            <Login  />
          </div>:
          null
      }
      </div>
      
    )

  }

	render() {
    return <div>{this.whatToShow()}</div>
	}
}
export default NavBar