// __tests__/CheckboxWithLabel-test.js

import React from 'react'
import {shallow, mount} from 'enzyme'
import Comments from '../Issues/comments-component'
import TestUtils from 'react-addons-test-utils'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import "mock-local-storage"
import Utils from '../utils-component'
console.error = () => {}

describe('Test issue comments component', () => {

    let mock_user
    let mock_issue
    let mock_component
    let mock_comment
    let utils = new Utils

    beforeEach(()=>{
        mock_user = {
          username: "testuser",
          issue_id: 1
        }

        mock_comment = {
        	text   : "This is a comment",
        	creator: mock_user.username,
        	id     : 1
        }

        mock_issue = {
          "title"      : "test issue",
          "description": "test description",
          "location"   : "test location",
          "creator"    : mock_user.username,
          "id"         : 1,
          "comments"   : [mock_comment]
        }
        // Mock Link route trigger
        mock_component = (data) => {
          return (<div></div>)
        }
        
    })
    let mockAdapter = new MockAdapter(axios)

	test('Test comments ul is shown', () => {
		const data = mount(
	    	<Comments issue_id={mock_issue.id} comments={mock_issue.comments} />
	  	)
      
      expect(data.find('ul').length).toBe(1)
	})

	test('Test comments add comment', () => {
	  // Simulate logged in
	  localStorage.token = "xyz"
	  const url = utils.urls.issue.comment + mock_issue.id + "/"
	  const data = mount(
	    <Comments issue_id={mock_issue.id} comments={mock_issue.comments} />
	  )
	  mockAdapter.onPost(url).reply(201, mock_comment)
    
	  data.find('#add_comment').simulate('click')
	  
	  data.instance().refs.comment.value = mock_comment.text
	  
	  data.find('#submit').simulate('submit')
    
	  data.setState({
      comment_added: true,
      comments_data: mock_issue.comments
    })
	  expect(data.html().indexOf("Comment Added")!==-1).toBeTruthy()
	})

})
