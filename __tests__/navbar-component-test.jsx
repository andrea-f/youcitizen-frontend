import NavBar from '../Nav/navbar-component'
import React from 'react'
import {shallow, mount} from 'enzyme'
import TestUtils from 'react-addons-test-utils'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import "mock-local-storage"
import {
  Link
} from 'react-router-dom'



describe('Test navabar component flow', () => {
	
	const mock_link = () => {
	    return (<div></div>)
	}

	Link.prototype.render = mock_link

	test('Test navbar is rendered with profile and logout buttons if user is logged in', () => {
	  
		localStorage.token = "xyz"
		
		const data = shallow(
			<NavBar />
		)
		
		expect(data.html().indexOf("Logout")!== -1).toBe(true)
	})

	test('Test navbar is rendered with login and signup buttons if user is not logged in', () => {
	  
		localStorage.token = null
		
		const data = shallow(
			<NavBar />
		)
		
		expect(data.html().indexOf("Signup")!== -1).toBe(true)
	})

})
