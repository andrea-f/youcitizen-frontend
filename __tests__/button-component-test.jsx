import NavButton from '../Nav/navbutton-component'
import React from 'react'
import {shallow} from 'enzyme'


test('Test button is rendered', () => {
  
  const spy = () => {
      expect(true).toBeTruthy()
  }

  const button_class = "button_class"

  const data = shallow(
    <NavButton buttonAction={spy} class_name={button_class} button_text="Hi" />
  )
  data.find('button').simulate('click')
  expect(data.html().indexOf(button_class) !== -1).toBeTruthy()
})