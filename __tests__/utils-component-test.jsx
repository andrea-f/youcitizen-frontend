import React from 'react'
import {mount} from 'enzyme'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import "mock-local-storage"
import Utils from '../utils-component'
import TestUtils from 'react-addons-test-utils'
import AddIssue from '../Issues/add-issue-component'
import {
  Link
} from 'react-router-dom'
// Disable console output


describe('Test Utils flow', () => {

	let mock_user
    let mock_issue
    let mock_component
    let mock_comment

    beforeEach(()=>{


        mock_user = {
          username: "testuser",
          issue_id: 1
        }



        mock_comment = {
          text   : "This is a comment",
          creator: mock_user.username,
          id     : 1
        }

        let comments_list = []
        comments_list.push(mock_comment)

        mock_issue = {
          "title"      : "test issue",
          "description": "test description",
          "location"   : "test location",
          "creator"    : mock_user.username,
          "id"         : 1,
          "comments"   : comments_list
        }
        // Mock Link route trigger
        mock_component = (data) => {
          return (<div></div>)
        }
        Link.prototype.render = mock_component
    })

	// Set localStorage token so logged in returns true
	localStorage.token = "xyz"
	const username = "username"
	const mockAdapter = new MockAdapter(axios)


	test('Test utils convert image to base64', () => {
	 
		const utils = new Utils
	  	// Mock a file on the filesystem
	  	const img = new File([new Blob()], "icon.png", {type:"image/png"})
	  	utils.getBase64(img).then((img64) => {
	        expect(img64.indexOf('base64') !== -1).toBeTruthy()
	        
	    }).catch(error => {
            console.log(error)
        })
	})

	test('Test session storage returns the correct username', () => {
		
		const utils = new Utils

		utils.setUsername(username)
		expect(utils.getUsername()).toBe(username)
	})

	test('Test session storage makes a new call if username is not in sessionStorage', () => {
		
		const utils = new Utils
		const url = utils.urls.user_profile
	  	mockAdapter.onGet(url).reply(200, {
	  		username: username,
            user_id : 1,
            issues  : true
	  	})

	  	const retrieved_username = utils.getUsername()
		expect(retrieved_username).toBe(username)
	})

	test('Test localStorage and sessionStorage are cleared on logout', () => {
		const utils = new Utils
		sessionStorage.username = username
	  	utils.logout()
	  	expect(typeof sessionStorage.username === 'undefined').toBeTruthy()
	  	expect(typeof localStorage.token === 'undefined').toBeTruthy()

	})


	test('Disabled property is set after form submit on submit button', () =>{
		const data = TestUtils.renderIntoDocument(
	    	<AddIssue 
	    	    issue={mock_issue} want_to_update={true} />
	    )

	    expect(data.refs.title.value).toBe(mock_issue.title)
	    const spy = () => {
	    	expect(true).toBeTruthy()
	    }
	    TestUtils.Simulate.submit(data.refs.submit)
	    // Check that update method has been called
	    AddIssue.prototype.updateIssue = spy
	}) 
})
