// __tests__/CheckboxWithLabel-test.js

import React from 'react'
import {shallow,mount} from 'enzyme'
import IssuesWrapper from '../Issues/issues-wrapper-component'
import Issues from '../Issues/issue-list-component'

import AddIssue from '../Issues/add-issue-component'
import {
  Link
} from 'react-router-dom'
import TestUtils from 'react-addons-test-utils'
import sinon from 'sinon'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import "mock-local-storage"
console.error = () => {}


describe('Test issue list component', () => {

    const mock_user = {
        username: "testuser",
        issue_id: 1
    }



    const mock_comment = {
        text   : "This is a comment",
        creator: mock_user.username,
       	id     : 1
    }

    let comments_list = []
    comments_list.push(mock_comment)

    const mock_issue = {
        "title"      : "test issue",
        "description": "test description",
        "location"   : "test location",
        "creator"    : mock_user.username,
        "id"         : 1,
        "comments"   : comments_list
    }
        


	test('Test issue wrapper renders with 1 elements if loading.', () => {
	  
	  const data = shallow(
	    <IssuesWrapper />
	  )
	  expect(data.children().length).toEqual(1);

	})

	test('Test issue wrapper renders with add issue buttons after rerendering.', () => {
	  
	  localStorage.token = "xyz"
	  sessionStorage.username = "username"

	  const data = shallow(
	    <IssuesWrapper />
	  )

	  expect(data.children().length).toEqual(1)
	  data.setState({request_returned:true})
	  //Confirm is present in html Add Issue
	  expect(data.html().indexOf('Add Issue')!==-1).toBeTruthy()

	})

	test('Test issue wrapper renders with one issue', () => {

	  Link.prototype.render = () => {
	  	return (<div></div>)
	  }
	  const data = mount(<IssuesWrapper />)
	    
	  data.setState({ 
	  	request_returned:true,
	  	data:[mock_issue]
	  })
	  //console.log(data.html())
	  expect(data.html().indexOf(mock_issue.location) !== -1).toBeTruthy()

	})

})
