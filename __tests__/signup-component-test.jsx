import React from 'react'
import {shallow, mount} from 'enzyme'
import Signup from '../Auth/signup-component'
import TestUtils from 'react-addons-test-utils'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import "mock-local-storage"
import Utils from '../utils-component'


describe('Signup flow', () => {

	const mock_signup_user = {
	  	"username": "test123",
	  	"password": "test456",
	  	"email"   : "test456@123four.com"
	}

	const utils = new Utils

	const mockAdapter = new MockAdapter(axios)

	test('Test signup component by checking for localStorage activated property', () => {
		// Instantiate component
		let signup_rendered = TestUtils.renderIntoDocument(<Signup />);
		// Mock user input
		signup_rendered.refs.username = {"value": mock_signup_user.username}
		signup_rendered.refs.password = {"value": mock_signup_user.password}
		signup_rendered.refs.email    = {"value": mock_signup_user.email}
		// Mock axios response
		mockAdapter.onPost(utils.urls.signup).reply(201, {
			username: mock_signup_user.username 
		});
		// Mock user submit
		TestUtils.Simulate.submit(signup_rendered.refs.submit)
		// Callback for when localStorage is accessed
		localStorage.itemInsertionCallback = (signed_up) => {
		  	// Check that localStorage.token exists.
		    expect(signed_up).toBeTruthy()
		  	expect(signup_rendered.props.username).toBeTruthy()
		}
	});

	test('Test error is raised in signup', () => {
	  	// Instantiate component
	  	const error = "Error in signup"
		let signup_rendered = mount(<Signup />)
		const spy = (e) => {
			expect(e.error).toBe(error)
		}
		// Mock utils format error
		// Spy on what it was called with
		utils.formatError = spy
		// Mock axios response
		mockAdapter.onPost(utils.urls.signup).reply(400, {
			error: error 
		})
		// Mock user submit
		signup_rendered.find('#submit').get(0).click()
	})


	test('Test user is activated if activation key is found in db', () => {
		
	})

})
