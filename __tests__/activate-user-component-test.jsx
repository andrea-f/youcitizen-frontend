import React from 'react'
import {mount} from 'enzyme'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import "mock-local-storage"
import ActivateUser from '../Auth/activate-user-component'
import Utils from '../utils-component'
console.error = () => {}


describe('Test activation user flow', () => {

	// Set localStorage token so logged in returns true
	localStorage.token = "xyz"
	const username = "username"
	const mockAdapter = new MockAdapter(axios)
	const utils = new Utils
	

	test('Test activation code results in successful activation', () => {
	
		const url  = utils.urls.activate_user       
	        mockAdapter.onPost(url).reply(200, {
	            email: "test123@email.com"
	        })

	    const code = {
	    	params: {
	    		code:"xyz"
	    	}
	    }

		const data = mount(<ActivateUser match={code} />)
		// Force setState as Enzyme doesn't reload the component after execution
		data.setState({activated:true})

		expect(data.html().indexOf('Your account has been activated') !== -1).toBeTruthy()
	})

	test('Test activation fails', () => {
	
		const url  = utils.urls.activate_user       
	        mockAdapter.onPost(url).reply(400, {
	            
	        })

	    const code = {
	    	params: {
	    		code:"xyz"
	    	}
	    }

		const data = mount(<ActivateUser match={code} />)
		// Force setState as Enzyme doesn't reload the component after execution
		data.setState({error:"ERROR"})
		
		expect(data.html().indexOf('ERROR') !== -1).toBeTruthy()
	})
})
