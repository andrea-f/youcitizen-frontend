// __tests__/CheckboxWithLabel-test.js

import React from 'react'
import IssueDetail from '../Issues/issue-detail-component'
import TestUtils from 'react-addons-test-utils'
import Utils from '../utils-component'
import MockAdapter from 'axios-mock-adapter'
import axios from 'axios'
import {
  Link
} from 'react-router-dom'
import "mock-local-storage"
import {mount} from 'enzyme'
import { Redirect } from 'react-router'
import { spyOnComponentMethod, stubComponentMethod } from 'sinon-spy-react'
import sinon from 'sinon'
console.error = () => {}

describe('Issue detail flow', () => {

    const utils = new Utils
    
    let mock_user
    let mock_issue
    let mock_component
    let mock_comment

    beforeEach(()=>{


        mock_user = {
          username: "testuser",
          issue_id: 1
        }



        mock_comment = {
          text   : "This is a comment",
          creator: mock_user.username,
          id     : 1
        }

        let comments_list = []
        comments_list.push(mock_comment)

        mock_issue = {
          "title"      : "test issue",
          "description": "test description",
          "location"   : "test location",
          "creator"    : mock_user.username,
          "id"         : 1,
          "comments"   : comments_list
        }
        // Mock Link route trigger
        mock_component = (data) => {
          return (<div></div>)
        }
        Link.prototype.render = mock_component
    })
    let mockAdapter = new MockAdapter(axios)
    const old_setState = IssueDetail.prototype.setState
    

    test('Test issue id is extracted from URL and request is made for issue', () => {
        const url  = utils.urls.issue.detail + mock_user.issue_id + "/"  

        // Mock url username parameter     
        const mock_trigger = {
            state:  {
                username: mock_user.username
            }
        }
        const mock_id = {
            params: {
                id: 1
            }
        }
        // Mocks set state
        const mock_setState = (data) => {
            expect(data.issue.title).toBe(mock_issue.title)
        }
        IssueDetail.prototype.setState = mock_setState        
        // Mock response
        mockAdapter.onGet(url).reply(200, mock_issue)
        const component = mount(<IssueDetail match={mock_id} />)
        // Restore initial component setState
        IssueDetail.prototype.setState = old_setState
    });

    test('Test issue is rendered in card if issue is passed in props', () => {
      
      // Mock renderer
      const component = mount(<IssueDetail issue={mock_issue} />)
      expect(component.find('.card-title').text()).toBe(mock_issue.title)

    });

    test('Test update and delete button are shown if username exists and matches issue creator', () => {
      localStorage.token = "xyz"
      sessionStorage.username = mock_user.username
      const component = mount(<IssueDetail 
        location={{
        state: {
          username: mock_user.username
        }
      }} issue={mock_issue} />)

      expect(
        (component.html().indexOf('Delete issue') !== -1 ?
        true: false)
      ).toBe(true)
      expect(
        (component.html().indexOf('Update issue') !== -1 ?
        true: false)
      ).toBe(true)
    });

    test('Test issue is deleted', () => {
      const url = utils.urls.delete + mock_issue.id
      mockAdapter.onDelete(url).reply(204)
      localStorage.token = "xyz"
      // Stub Redirect perform lifecycle method
      Redirect.prototype.perform = () => {
        expect(true).toBe(true)
      }
      const data = mount(
        <IssueDetail
         issue={mock_issue} 
         location={{
          state: {
            username: mock_user.username
          }
        }} />
      )
      data.find('#delete').simulate('click')
    })

    test.skip('Test issue is update', () => {
      
      const data = shallow(
        <AddIssue />
      )
    })
})