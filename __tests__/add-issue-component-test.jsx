import Utils from '../utils-component'
import React from 'react'
import AddIssue from '../Issues/add-issue-component'
import TestUtils from 'react-addons-test-utils'
import "mock-local-storage"
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'


describe('Add issue detail flow', () => {

	// Mock axios response
	const mockAdapter = new MockAdapter(axios)
	const utils = new Utils

	const mock_issue = {
		"title"      : "test issue",
		"description": "test description",
		"location"   : "test location"
	}

	const mock_response = {
		"title"      : mock_issue.title,
		"description": mock_issue.description,
		"location"   : mock_issue.location
	}	

	test('Test Form is correctly rendered with already populated data', () => {
	    const data = TestUtils.renderIntoDocument(
	    	<AddIssue 
	    	    issue={mock_issue} want_to_update={true} />
	    )

	    expect(data.refs.title.value).toBe(mock_issue.title)
	    const spy = () => {
	    	expect(true).toBeTruthy()
	    }
	    TestUtils.Simulate.submit(data.refs.submit)
	    // Check that update method has been called
	    AddIssue.prototype.updateIssue = spy
	});

	test('Test add new issue', () => {
		
		const spy = (data) => {
			// Check that returned matches input.
			expect(data).toBeTruthy()
		}
	  	// Instantiate component
	  	const add_issue = TestUtils.renderIntoDocument(<AddIssue updateCallback={spy} />);
	  	
	  	// Set issue form values
	  	add_issue.refs.title       = {"value": mock_issue.title}
		add_issue.refs.description = {"value": mock_issue.description}
		add_issue.refs.location    = {"value": mock_issue.location}
		// Mock POST response
		mockAdapter.onPost(utils.urls.issue.add).reply(201, mock_response);
		// Mock user submit
		TestUtils.Simulate.submit(add_issue.refs.submit)

	})


})
