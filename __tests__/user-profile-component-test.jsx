// __tests__/CheckboxWithLabel-test.js

import React from 'react'
import {shallow, mount} from 'enzyme'
import UserProfile from '../Auth/user-profile-component'
import TestUtils from 'react-addons-test-utils'
import "mock-local-storage"
import Utils from '../utils-component'
import MockAdapter from 'axios-mock-adapter'
import axios from 'axios'
import IssuesWrapper from '../Issues/issues-wrapper-component'


describe('UserProfile flow', () => {

    const mockAdapter = new MockAdapter(axios)
    const utils = new Utils

    test('Test passing username via URL makes the call to getProfile', () => {
      	const mock_user = {
      		username: "testuser"
      	}
        // Mocks Router data passed in via url
        const mock_trigger = {params: {username:mock_user.username}}

        const spy = (data) => {
          // Check that returned matches input.
          expect(data).toBe(mock_user.username)
        }
        // Instantiate component
        const component = TestUtils.renderIntoDocument(<UserProfile match={mock_trigger} getProfile = {spy} />)

    });

    test('Test passing username via lcoation property of route Link makes the call to getProfile', () => {
        const mock_user = {
            username: "testuser"
        }
        // Mock link trigger
        const mock_trigger = {
            state:  {
                username:mock_user.username
            }
        }
        const spy = (data) => {
          // Check that returned matches input.
          expect(data).toBe(mock_user.username)
        }
        const url  = utils.urls.user_profile + mock_user.username       
        mockAdapter.onGet(url).reply(200, {
            username: mock_user.username,
            user_id : mock_user.id,
        })
        // Instantiate component
        const component = TestUtils.renderIntoDocument(<UserProfile location={mock_trigger} getProfile = {spy} />)
    });

    test('Test issues in user profile is true', () => {

        
        // Mock link trigger
        const mock_user = {
            username: "testuser",
            id      : 1
        }
        const url  = utils.urls.user_profile + mock_user.username       
        const mock_trigger = {
            state:  {
                username:mock_user.username
            }
        }
        
        
        // Callback mocking issue wrapper componentDidMount
        const spy = () => {
          expect(true).toBeTruthy()
        }
        IssuesWrapper.prototype.componentDidMount = spy
        mockAdapter.onGet(url).reply(200, {
            username: mock_user.username,
            user_id : mock_user.id,
        })
        const component = TestUtils.renderIntoDocument(<UserProfile location={mock_trigger} />)
    })
})