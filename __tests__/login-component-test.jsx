import React from 'react'
import {shallow} from 'enzyme'
import Login from '../Auth/login-component'
import TestUtils from 'react-addons-test-utils'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import "mock-local-storage"


describe('Login and auth flow', () => {

	const mockAdapter = new MockAdapter(axios);
	    
	test('Test login fails without credentials component by checking for localStorage token', () => {
	  

		const data = shallow(
		    <Login  />
		)
		data.find('#submit').simulate('click')
		expect(localStorage.token).toBe(undefined)
	})

	test('Test login succeed component by checking for localStorage token', () => {

	    // Instantiate component
	    let login_rendered = TestUtils.renderIntoDocument(<Login  />)
	    // Mock user input
	    login_rendered.refs.username = {"value": "test123"}
	    login_rendered.refs.password = {"value": "test456"}
	    // Mock axios response
	    mockAdapter.onPost('http://127.0.0.1:8000/api-token-auth/').reply(200, {
	       id: 1, username: 'test123', token: 'xyz' 
	    })
	    // Mock user submit
	    TestUtils.Simulate.submit(login_rendered.refs.submit)
	    // Callback for when localStorage is accessed
	    localStorage.itemInsertionCallback = (user_info) => {
	  		// Check that localStorage.token exists.
	  		expect(user_info.token).toBeTruthy()
	  }
	});
})
