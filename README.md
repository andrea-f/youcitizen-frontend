### What is this repository for ###

This repo is the frontend code for the YouCitizen service. It provides a REST UI built with React 15.0.0 and Bootstrap in ES6. 
Considering the size of the project, the routing is done with React Router 4.0.0, JWT Authentication. Testing is done with JEST, built with Gulp and Browserify. Tested on Chrome 57.~ and Firefox 54.~

### How to get up and running ###

* Clone the project
* Have npm already installed on your system
* Cd in the directory of the repository
* Run `npm install`
* To run tests: `npm test`
* To build project run: `gulp`, it will create a dist folder with the bundled js inside.
* Point a webserver to the root folder, containing `index.html`
* The point browser to http://IP:PORT running the webserver. 

### Staging ###

The bundled js has been hosted via Apache on Ubuntu on a EC2 Instance. 
It can be accessed here: http://youcitizen.radiolondra.co.uk

### TODO FOR FUTURE ###

* Add pagination if results are more than a predefined number
* Add tags and locations components to allow searching by categories
* ~~Add a spinner when exchanging data with server~~ [625e52b1f7a2bf9fd1b08ae430f89e9304ac8acc](https://bitbucket.org/andrea-f/youcitizen-frontend/commits/625e52b1f7a2bf9fd1b08ae430f89e9304ac8acc)