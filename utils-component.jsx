import React from 'react'
import axios from 'axios'
import { Redirect } from 'react-router'

class Utils extends React.Component {

  constructor(props) {
      super(props);
      this.urls = {
        /* 
        API Endpoints 
        Public http://youcitizen.radiolondra.co.uk
        */
        activate_user: "http://127.0.0.1:8000/users/activate/",
        login         : "http://127.0.0.1:8000/api-token-auth/",
        signup        : "http://127.0.0.1:8000/users/signup/",
        issue         : {
          add         : "http://127.0.0.1:8000/issues/",
          detail      : "http://127.0.0.1:8000/issue/",
          every       : "http://127.0.0.1:8000/issues/",
          update      : "http://127.0.0.1:8000/issue/",
          comment     : "http://127.0.0.1:8000/issue/add_comment/"
        },
        user_profile  : "http://127.0.0.1:8000/user/",
        /* Third parties API End points */
        maps_api_key  : "AIzaSyDxaqzmYAd6pkQSB26yD4rFUnQRafXW0oI",
        maps_api_url  : "https://www.google.com/maps/embed/v1/place?key="
      }
      this.isLoggedIn    = this.isLoggedIn.bind(this)
      this.formatError   = this.formatError.bind(this)
      this.getUsername   = this.getUsername.bind(this)
      this.setUsername   = this.setUsername.bind(this)
      this.logout        = this.logout.bind(this)
      this.setLoginToken = this.setLoginToken.bind(this)
      
  }

  _addToRequestHeader() {
    if (this.isLoggedIn()) {
      // if auth token is present, then add it to the request headers
      axios.defaults.headers.common['Authorization'] = "Token "+localStorage.token; 
    }
  }

  _showSpinner() {
    const forms = document.getElementsByTagName('form')
    if (forms.length > 0) {
        // Disable submit button after click
        forms[0].querySelector("#submit").disabled = true
        
        const node = document.createElement("div")
        node.id = "loading"
        node.innerHTML = "<pre>LOADING!</pre>"   

        forms[0].appendChild(node)


    }
  }

  _hideSpinner() {
    const spinner = document.getElementById('loading')
    if (spinner) {
      spinner.style.display = "none"
      spinner.parentNode.removeChild(spinner)
    }

    const forms = document.getElementsByTagName('form')
    // Re enable submit button in case there was a form error
    if (forms.length > 0) {
        // Disable submit button after click
        forms[0].querySelector("#submit").disabled = false
    }
  }

  // Redirect on not authorized
  _checkForInvalidToken(error) {
    // If there was some token/suth issue
   if (error.response.status === 401) {
        // clear any residuals
        this.logout()
        // Then refresh
        window.location.reload()
    }
  }

  // Wrapper for DELETE request
  deleteResource(url) {
    this._addToRequestHeader()
        return new Promise(resolve => {
          this._showSpinner()
            axios.delete(url)
            .then(res => {
              this._hideSpinner()
                return resolve(res.data);
            }).catch(error => {
                this._hideSpinner()
                this._checkForInvalidToken(error)
                return resolve(error)
            })
        })
  }

  // Wrapper for PATCH request
  updateResource(url, data) { 
      this._addToRequestHeader()
        return new Promise(resolve => {
          this._showSpinner()
            axios.patch(url, data)
            .then(res => {
              this._hideSpinner()
                return resolve(res.data);
            })
            .catch(error => {
              this._hideSpinner()
              this._checkForInvalidToken(error)
                return resolve(error)
            })
        })
  }

  // Wrapper for GET request
	getResource(url) { 
      this._addToRequestHeader()
        return new Promise(resolve => {
          this._showSpinner()
            axios.get(url)
          	.then(res => {
              this._hideSpinner()
                return resolve(res.data);
          	})
            .catch(error => {
              this._hideSpinner()
              this._checkForInvalidToken(error)
                return resolve(error)
            })
        })
  }



  // Wrapper for POST request
  postResource(url, data) {
    this._addToRequestHeader()
      return new Promise(resolve => { 
        this._showSpinner()
          axios.post(url,data)
          .then(res => {
            this._hideSpinner()
              return resolve(res.data)
          })
          .catch(error => {
            this._hideSpinner()
              this._checkForInvalidToken(error)
              return resolve(error)
          })
      })
  }

  // Traverses error obj and prints it as a string
  formatError(error_obj) {
    let error_str = ""
    for (let r in error_obj) {
        error_str += r+": "+error_obj[r] + "\n"
    }
    return error_str
  }

  // Converts image file object to base64
  getBase64(file_input) {
      return new Promise(resolve => {
        const reader = new FileReader();
        
        reader.onload = function () {
          return resolve(reader.result)
        }
        reader.onerror = function (error) {
          return resolve(error)
        }
        reader.readAsDataURL(file_input);
      })
      
    }

  // Checks if token is present
  isLoggedIn() {
    if (localStorage.token) return true;
    return false
  }

  // Gets username from session storage
  // Or makes a new request
  getUsername() {
    if (this.isLoggedIn()) {
        if (sessionStorage) {
            if (typeof sessionStorage.username !== 'undefined') {
                return sessionStorage.username
            } else {
                // Make fetch for username
                const url  = this.urls.user_profile + "me/"
                this.getResource(url).then((data) => {
                    if (typeof data.username !== 'undefined') {
                        this.setUsername(data.username)
                        return data.username
                    } else {
                        return null
                    }
                })
            }
        } else {
          return null
        }
    } else {
      return null
    }
  }

  // Sets username in session storage
  setUsername(username) {
    if (sessionStorage) {
        sessionStorage.username = username
        return true
    } else {
        return false
    }
      
  }

  // Sets JWT token in localStorage
  // Default expiry date is 7 days
  setLoginToken(token) {
      if (localStorage) {
          localStorage.token = token
          return true
      } else {
          return false
      }
  }

  // Clear everything
  logout() {
    console.log("loggingout")
      if (sessionStorage) sessionStorage.clear()
      if (localStorage) localStorage.clear()
      return true
  }


  showSuccess(data) {
    return (<pre>{data}</pre>)
  }

  showError(data) {
    return (<pre>{data}</pre>)
  }

  render(){return null}
}


export default Utils
